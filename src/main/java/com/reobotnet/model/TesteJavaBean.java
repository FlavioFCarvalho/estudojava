package com.reobotnet.model;

public class TesteJavaBean {

	public static void main(String[] args) {
		
		PessoaBean pessoa = new PessoaBean();
		
		
		pessoa.setNome("Maria");
		pessoa.setIdade(25);
		
		System.out.println("O nome dela é " + pessoa.getNome() + " e a idade dela é:" + pessoa.getIdade());
	}

}
