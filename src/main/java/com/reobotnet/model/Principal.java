package com.reobotnet.model;

public class Principal {

	public static void main(String[] args) {
		
		Carro  meuCarro = new Carro();
		
		meuCarro.fabricante = "Fiat";
		meuCarro.modelo = "Palio";
		meuCarro.cor    = "Branco";
		meuCarro.anoDeFabricacao = 1999;
		
		System.out.println("Meu Carro");
		System.out.println("----------");
		System.out.println("Modelo: " + meuCarro.modelo);
		System.out.println("Cor: " + meuCarro.cor);
		
		
		
		Carro  seuCarro = new Carro();
		
		seuCarro.fabricante = "Chevrolet";
		seuCarro.modelo     = "Onix";
		seuCarro.cor        = "Preto";
		seuCarro.anoDeFabricacao = 2000;
		
		System.out.println("");
		System.out.println("XXXXXXXXXXXXXXXXXXXX");
		System.out.println("");
		System.out.println("Seu Carro");
		System.out.println("----------");
		System.out.println("Modelo: " + seuCarro.modelo);
		System.out.println("Cor: " + seuCarro.cor);
		
	}

}
