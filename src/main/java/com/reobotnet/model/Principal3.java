package com.reobotnet.model;

public class Principal3 {
	
	public static void main(String[] args) {
		Paciente p = new Paciente();
		p.peso = 69;
		p.altura = 1.65;
		
		IMC imc = p.calcularIndiceDeMassaCorporal();
		
		System.out.println("O Seu Imc é: " + imc.indice);
		System.out.println("Abaixo do peso ideal: " + imc.abaixoDoPesoIdeal);
		System.out.println("Peso ideal: " + imc.pesoIdeal);
		System.out.println("Obeso: " + imc.obeso);
		System.out.println("Grau de obesidade: " + imc.grauObesidade);
	}

}
