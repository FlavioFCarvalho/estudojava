package com.reobotnet.model;

public class TesteArrayComCarro {

	public static void main(String[] args) {
		
		Carro[] carros = new Carro[4];
		
		carros[0] = new Carro();
		carros[0].anoDeFabricacao = 2011;
		
		carros[3] = new Carro();
		carros[3].biCombustivel = true;
		
		System.out.println("Ano fabricacao: " + carros[0].anoDeFabricacao);
		
		System.out.println("O carro 3 é bi-combustível? : " + carros[3].biCombustivel);
		

	}

}
